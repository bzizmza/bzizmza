# Hello! I'm Bima Setyo.  <img src="https://raw.githubusercontent.com/Tarikul-Islam-Anik/Animated-Fluent-Emojis/master/Emojis/Animals/Cat%20Face.png" alt="Cat Face" width="25" height="25" />
💻 Front-end Developer, Data Scientist, and Open Source Enthusiast.<br>
🎓 Brawijaya University, Computer Engineering.

## **What can I do?**
In recent years, I've been learning new technologies and languages. These are the ones I've learned so far.

[![Bima's Github most used language](https://github-readme-stats.vercel.app/api/top-langs/?username=bzizmza&layout=compact&hide_progress=true)](http://github.com/bzizmza)

## **Let's Connect with Me. :)**
Say hi to me on social that I have.

[![Portofolio Site Badge](https://img.shields.io/badge/website-000000?style=for-the-badge&logo=About.me&logoColor=white)](https://bimasetyo.com)
[![Twitter Badge](https://img.shields.io/badge/Twitter-1DA1F2?style=for-the-badge&logo=twitter&logoColor=white)](https://www.twitter.com/bzizmza)
[![LinkedIn Badge](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/abimanyusrisetyo/)

[![Bima's GitHub stats](https://github-readme-stats.vercel.app/api?username=bzizmza)](http://github.com/bzizmza)

[![LeetCode Stats](https://leetcard.jacoblin.cool/bzizmza?theme=light&font=Noto%20Sans)](https://leetcode.com/bzizmza)
